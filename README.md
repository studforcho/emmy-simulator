# Tezos protocol simulator

This software allows to simulate non-standard baking strategies.

Type `make` to build.

Usage: `./simu.exe` should display a list of available commands.
The binary functions in two modes:
- data generation
For instance,
```
./simu sample -a iar-attacker -p emmy+C -s 879874 -n 300
```
will generate data for the adversary called 'two-step-profitable-selfish' on protocol 'emmy+C'
with seed 879874 and 300 random chains per value of the adversarial stake.
This will generate a file called `two-step-profitable-selfish-emmy+C-1.0-1.0-87987.trace`

- plotting indicators
```
/simu.exe -- compute AR on ./two-step-profitable-selfish-emmy+C-1.0-1.0-87987.trace --show
```
will compute the AR indicator for the provided file(s). The display system will gather
all adversaries in a given protocol in the sample plot. The --show option triggers displaying
the result, in all cases a .png file is produced with the result.
