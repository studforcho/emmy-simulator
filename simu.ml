(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2019 Nomadic Labs. <nomadic@tezcore.com>                    *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(* ------------------------------------------------------------------------- *)
(* ------------------------------------------------------------------------- *)
(* Command-line parsing *)

type chain_termination = Height of {height : int} | Age of {seconds : float}

type adversary =
  | Not_dishonest
  | Naive_block_stealing
  | AR_one_step
  | Deflationary
  | IAR_attacker
  | AR_two_step
  | RSD_attacker

(* aka "Eff" *)

type protocol = Emmy_plus | Emmy_sharp

type indicator = AR | IAR | RSD | AR_vs_honest

let all_adversaries =
  [ Not_dishonest;
    Naive_block_stealing;
    AR_one_step;
    Deflationary;
    IAR_attacker;
    AR_two_step;
    RSD_attacker ]

let all_protocols = [Emmy_plus; Emmy_sharp]

let all_indicators = [AR; IAR; RSD; AR_vs_honest]

type mode =
  | Generate_data of sampling_parameters
  | Compute_indicator of {
      indicator : indicator;
      data : string list;
      show : bool;
    }

and sampling_parameters = {
  adversary : adversary;
  protocol : protocol;
  undelegated : float;
  (* in [0,1]*)
  dishonest_health : float;
  honest_health : float;
  seed : int option;
  nsamples : int;
  debug : bool;
}

(* ------------------------------------------------------------------------- *)

let pp_adversary fmtr (a : adversary) =
  match a with
  | Not_dishonest ->
      Format.fprintf fmtr "cooperative"
  | Naive_block_stealing ->
      Format.fprintf fmtr "naive-block-stealing"
  | AR_one_step ->
      Format.fprintf fmtr "weak-ar-attacker"
  | AR_two_step ->
      Format.fprintf fmtr "ar-attacker"
  | Deflationary ->
      Format.fprintf fmtr "deflationary"
  | IAR_attacker ->
      Format.fprintf fmtr "iar-attacker"
  | RSD_attacker ->
      Format.fprintf fmtr "rsd-attacker"

let pp_protocol fmtr (p : protocol) =
  match p with
  | Emmy_plus ->
      Format.fprintf fmtr "emmy+B"
  | Emmy_sharp ->
      Format.fprintf fmtr "emmy+C"

let pp_indicator fmtr (i : indicator) =
  match i with
  | AR ->
      Format.fprintf fmtr "AR"
  | IAR ->
      Format.fprintf fmtr "IAR"
  | RSD ->
      Format.fprintf fmtr "RSD"
  | AR_vs_honest ->
      Format.fprintf fmtr "AR-vs-honest"

let parse_adversary s =
  match s with
  | "cooperative" ->
      Ok Not_dishonest
  | "naive-block-stealing" ->
      Ok Naive_block_stealing
  | "weak-ar-attacker" ->
      Ok AR_one_step
  | "ar-attacker" ->
      Ok AR_two_step
  | "deflationary" ->
      Ok Deflationary
  | "iar-attacker" ->
      Ok IAR_attacker
  | "rsd-attacker" ->
      Ok RSD_attacker
  | _ ->
      Error (`Msg "parse_adversary: error")

let parse_protocol s =
  match s with
  | "emmy+B" ->
      Ok Emmy_plus
  | "emmy+C" ->
      Ok Emmy_sharp
  | _ ->
      Error (`Msg "parse_protocol: error")

let parse_indicator s =
  match s with
  | "IAR" ->
      Ok IAR
  | "AR" ->
      Ok AR
  | "RSD" ->
      Ok RSD
  | "AR-vs-honest" ->
      Ok AR_vs_honest
  | _ ->
      Error (`Msg "parse_indicator: error")

let rec pp_mode fmtr (m : mode) =
  let open Format in
  match m with
  | Generate_data sp ->
      fprintf fmtr "Generating data:@.%a" pp_sampling_parameters sp
  | Compute_indicator {indicator; data; show = _} ->
      fprintf
        fmtr
        "Computing indicator %a on %a@"
        pp_indicator
        indicator
        (pp_print_list pp_print_string)
        data

and pp_sampling_parameters fmtr (sp : sampling_parameters) =
  let open Format in
  fprintf fmtr "{ " ;
  open_vbox 0 ;
  fprintf fmtr "adversary = %a;@," pp_adversary sp.adversary ;
  fprintf fmtr "protocol = %a;@," pp_protocol sp.protocol ;
  fprintf fmtr "undelegated = %f;@," sp.undelegated ;
  fprintf fmtr "dishonest health = %f;@," sp.dishonest_health ;
  fprintf fmtr "honest health = %f;@," sp.honest_health ;
  fprintf fmtr "nsamples = %d;@," sp.nsamples ;
  fprintf
    fmtr
    "seed = %a;@,"
    (pp_print_option
       ~none:(fun fmtr () -> pp_print_string fmtr "self-init")
       pp_print_int)
    sp.seed ;
  fprintf fmtr "debug = %a }@." pp_print_bool sp.debug

let adversary_encoding : adversary Data_encoding.t =
  let open Data_encoding in
  union
    [ case
        ~title:"not-dishonest"
        (Tag 0)
        unit
        (function Not_dishonest -> Some () | _ -> None)
        (fun _ -> Not_dishonest);
      case
        ~title:"naive-block-stealing"
        (Tag 1)
        unit
        (function Naive_block_stealing -> Some () | _ -> None)
        (fun _ -> Naive_block_stealing);
      case
        ~title:"one-step-block-stealing"
        (Tag 2)
        unit
        (function AR_one_step -> Some () | _ -> None)
        (fun _ -> AR_one_step);
      case
        ~title:"deflationary"
        (Tag 3)
        unit
        (function Deflationary -> Some () | _ -> None)
        (fun _ -> Deflationary);
      case
        ~title:"iar-attacker"
        (Tag 4)
        unit
        (function IAR_attacker -> Some () | _ -> None)
        (fun _ -> IAR_attacker);
      case
        ~title:"two-step-block-stealing"
        (Tag 5)
        unit
        (function AR_two_step -> Some () | _ -> None)
        (fun _ -> AR_two_step);
      case
        ~title:"rsd-attacker"
        (Tag 6)
        unit
        (function RSD_attacker -> Some () | _ -> None)
        (fun _ -> RSD_attacker) ]

let protocol_encoding : protocol Data_encoding.t =
  let open Data_encoding in
  union
    [ case
        ~title:"emmy-plus"
        (Tag 0)
        unit
        (function Emmy_plus -> Some () | _ -> None)
        (fun _ -> Emmy_plus);
      case
        ~title:"emmy-sharp"
        (Tag 1)
        unit
        (function Emmy_sharp -> Some () | _ -> None)
        (fun _ -> Emmy_sharp) ]

(* ------------------------------------------------------------------------- *)

let default_sampling_parameters =
  {
    adversary = Naive_block_stealing;
    protocol = Emmy_plus;
    undelegated = 0.2;
    dishonest_health = 1.0;
    honest_health = 1.0;
    seed = None;
    nsamples = 3000;
    debug = false;
  }

let default_show_value = false

let usage () =
  let open Format in
  printf "usage: %s <command> [options]@." Sys.argv.(0) ;
  (* sample command *)
  printf "%s sample " Sys.argv.(0) ;
  open_vbox 0 ;
  printf
    "[{-a|--adversary} {<adversary>}] (default = %a)@,"
    pp_adversary
    default_sampling_parameters.adversary ;
  printf
    "[{-p|--protocol} {<protocol>}] (default = %a)@,"
    pp_protocol
    default_sampling_parameters.protocol ;
  printf "[{--adversary-health} <float, [0,1]>] (default = 1.0)@," ;
  printf "[{--honest-health} <float, [0,1]>] (default = 1.0)@," ;
  printf "[{-s|--seed} <int>] (self-init if not specified)@," ;
  printf
    "[{-n|--nsamples} <int>] (default = %d)@,"
    default_sampling_parameters.nsamples ;
  printf
    "[{-d|--debug}] (default = %s)@."
    (string_of_bool default_sampling_parameters.debug) ;
  printf "@." ;
  (* compute *)
  printf "%s compute <indicator> on <files> " Sys.argv.(0) ;
  open_vbox 0 ;
  printf "[--show] (default = %s)@." (string_of_bool default_show_value) ;
  close_box () ;
  printf "%s list protocols@." Sys.argv.(0) ;
  printf "%s list adversaries@." Sys.argv.(0) ;
  printf "%s list indicators@." Sys.argv.(0) ;
  printf "@." ;
  printf
    "Example: to simulate the naive block-stealing baking scenario with %a, \
     run:@."
    pp_protocol
    Emmy_plus ;
  printf
    "%s simulate -a naive-block-stealing -p %a@."
    Sys.argv.(0)
    pp_protocol
    Emmy_plus

let set_if_some result x f =
  match x with None -> result | Some x -> f result x

let filter_files strings =
  List.filter
    (fun string ->
      if String.length string = 0 then assert false
      else
        match string.[0] with
        | '-' ->
            false
        | _ -> (
          match Unix.stat string with
          | exception _ ->
              false
          | {Unix.st_kind = S_REG; _} ->
              true
          | _ ->
              false ))
    strings

let parse_commandline () =
  let open Minicli in
  let open Format in
  let (argc, args) = CLI.init () in
  if argc <= 1 then (usage () ; exit 1)
  else
    match List.tl args with
    | "sample" :: _ ->
        let adv_str_opt = CLI.get_string_opt ["-a"; "--adversary"] args in
        let proto_str_opt = CLI.get_string_opt ["-p"; "--protocol"] args in
        let adv_health_opt = CLI.get_float_opt ["--adversary-health"] args in
        let hon_health_opt = CLI.get_float_opt ["--honest-health"] args in
        let seed_opt = CLI.get_int_opt ["-s"; "--seed"] args in
        let nsamples_opt = CLI.get_int_opt ["-n"; "--nsamples"] args in
        let debug = CLI.get_set_bool ["-d"; "--debug"] args in
        let result = default_sampling_parameters in
        let result =
          set_if_some result adv_str_opt (fun result adv_str ->
              match parse_adversary adv_str with
              | Ok adversary ->
                  {result with adversary}
              | Error (`Msg _msg) ->
                  eprintf "could not parse adversary@." ;
                  usage () ;
                  exit 1)
        in
        let result =
          set_if_some result proto_str_opt (fun result proto_str ->
              match parse_protocol proto_str with
              | Ok protocol ->
                  {result with protocol}
              | Error (`Msg _msg) ->
                  eprintf "could not parse protocol@." ;
                  usage () ;
                  exit 1)
        in
        let result =
          set_if_some result adv_health_opt (fun result dishonest_health ->
              if dishonest_health < 0.0 || dishonest_health > 1.0 then (
                eprintf "wrong adversary health: should be in [0,1]" ;
                usage () ;
                exit 1 )
              else {result with dishonest_health})
        in
        let result =
          set_if_some result hon_health_opt (fun result honest_health ->
              if honest_health < 0.0 || honest_health > 1.0 then (
                eprintf "wrong honest health: should be in [0,1]" ;
                usage () ;
                exit 1 )
              else {result with honest_health})
        in
        let result = {result with seed = seed_opt} in
        let result =
          match nsamples_opt with
          | None ->
              result
          | Some nsamples ->
              {result with nsamples}
        in
        Generate_data {result with debug}
    | "compute" :: indicator :: "on" :: files -> (
      match parse_indicator indicator with
      | Ok indicator ->
          let data = filter_files files in
          let show = CLI.get_set_bool ["--show"] args in
          Compute_indicator {indicator; data; show}
      | Error (`Msg _msg) ->
          eprintf "could not parse indicator@." ;
          usage () ;
          exit 1 )
    | "compute" :: _ ->
        printf "Wrong `compute` command@." ;
        usage () ;
        exit 1
    | "list" :: _ ->
        ( if argc <= 2 then (
          printf "Incomplete `list` command@." ;
          usage () )
        else
          match Sys.argv.(2) with
          | "protocols" ->
              List.iter (fun s -> printf "%a@." pp_protocol s) all_protocols
          | "adversaries" ->
              List.iter (fun a -> printf "%a@." pp_adversary a) all_adversaries
          | "indicators" ->
              List.iter (fun a -> printf "%a@." pp_indicator a) all_indicators
          | _ ->
              printf "Invalid `list` command@." ;
              usage () ) ;
        exit 1
    | _ ->
        eprintf "Ill-formed command.@." ;
        usage () ;
        exit 1

let mode = parse_commandline ()

(* ------------------------------------------------------------------------- *)

let debug : ('a, Format.formatter, unit) format -> 'a =
  match mode with
  | Generate_data {debug; _} when debug ->
      Format.eprintf
  | _ ->
      fun fmt -> Format.ifprintf Format.err_formatter fmt

(* ------------------------------------------------------------------------- *)
(* ------------------------------------------------------------------------- *)
(* ------------------------------------------------------------------------- *)
(* Block-Stealing baking simulation. *)

open StaTz

type players = Honest | Dishonest

type rewards = {max_rewards : float; total_hon : float; total_dis : float}

type chain_statistics = {
  rewards : rewards;
  steal_proba : float;
  stolen_rewards : float;
  length : float;
}

type simulation_results = {
  empirical : chain_statistics Stats.emp;
  honest_share : float;
  dishonest_share : float;
}

type savedata = sampling_parameters * simulation_results list

let rewards_encoding : rewards Data_encoding.t =
  let open Data_encoding in
  conv
    (fun {max_rewards; total_hon; total_dis} ->
      (max_rewards, total_hon, total_dis))
    (fun (max_rewards, total_hon, total_dis) ->
      {max_rewards; total_hon; total_dis})
    (tup3 float float float)

let chain_statistics_encoding : chain_statistics Data_encoding.t =
  let open Data_encoding in
  conv
    (fun {rewards; steal_proba; stolen_rewards; length} ->
      (rewards, steal_proba, stolen_rewards, length))
    (fun (rewards, steal_proba, stolen_rewards, length) ->
      {rewards; steal_proba; stolen_rewards; length})
    (tup4 rewards_encoding float float float)

let chain_statistics_emp_encoding : chain_statistics Stats.emp Data_encoding.t
    =
  let open Data_encoding in
  conv
    (fun emp ->
      let (`Empirical data) = Stats.raw_data_empirical emp in
      data)
    Stats.empirical_of_raw_data
    (array chain_statistics_encoding)

let simulation_results_encoding : simulation_results Data_encoding.t =
  let open Data_encoding in
  conv
    (fun {empirical; honest_share; dishonest_share} ->
      (empirical, honest_share, dishonest_share))
    (fun (empirical, honest_share, dishonest_share) ->
      {empirical; honest_share; dishonest_share})
    (tup3 chain_statistics_emp_encoding float float)

let sampling_parameters_encoding : sampling_parameters Data_encoding.t =
  let open Data_encoding in
  conv
    (fun { adversary;
           protocol;
           undelegated;
           dishonest_health;
           honest_health;
           seed;
           nsamples;
           debug } ->
      let seed = Option.map Int64.of_int seed in
      ( adversary,
        protocol,
        undelegated,
        dishonest_health,
        honest_health,
        seed,
        nsamples,
        debug ))
    (fun ( adversary,
           protocol,
           undelegated,
           dishonest_health,
           honest_health,
           seed,
           nsamples,
           debug ) ->
      let seed = Option.map Int64.to_int seed in
      {
        adversary;
        protocol;
        undelegated;
        dishonest_health;
        honest_health;
        seed;
        nsamples;
        debug;
      })
    (tup8
       adversary_encoding
       protocol_encoding
       float
       float
       float
       (option int64)
       int31
       bool)

let savedata_encoding : savedata Data_encoding.t =
  let open Data_encoding in
  tup2 sampling_parameters_encoding (list simulation_results_encoding)

let read_file fn = Lwt_io.with_file fn ~mode:Input (fun ch -> Lwt_io.read ch)

let create_file ?(perm = 0o644) name content =
  let open Lwt in
  Lwt_unix.openfile name Unix.[O_TRUNC; O_CREAT; O_WRONLY] perm
  >>= fun fd ->
  Lwt_unix.write_string fd content 0 (String.length content)
  >>= fun _ -> Lwt_unix.close fd

let load_simulation_results : string -> savedata =
 fun filename ->
  let open Lwt in
  Lwt_main.run
  @@ ( read_file filename
     >>= fun str ->
     Format.eprintf "load_simulation_results: loaded %s@." filename ;
     let bytes = Bytes.unsafe_of_string str in
     match Data_encoding.Binary.of_bytes savedata_encoding bytes with
     | Some result ->
         Lwt.return result
     | None ->
         Printf.eprintf "load_simulation_results: can't load file; exiting" ;
         exit 1 )

let save_simulation_results : string -> savedata -> unit =
 fun filename results ->
  let bytes =
    match Data_encoding.Binary.to_bytes_exn savedata_encoding results with
    | exception Data_encoding.Binary.Write_error err ->
        let msg =
          Format.asprintf
            "save_simulation_results: encoding failed (%a)"
            Data_encoding.Binary.pp_write_error
            err
        in
        failwith msg
    | res ->
        res
  in
  Lwt_main.run @@ create_file filename (Bytes.unsafe_to_string bytes)

let instantiate_players ~honest_health ~dishonest_health ~honest_share
    ~dishonest_share =
  ( module struct
    type t = players

    let all = [|Honest; Dishonest|]

    let compare (x : t) (y : t) =
      match (x, y) with
      | (Honest, Honest) | (Dishonest, Dishonest) ->
          0
      | (Honest, Dishonest) ->
          -1
      | (Dishonest, Honest) ->
          1

    let pp fmtr = function
      | Honest ->
          Format.pp_print_string fmtr "Honest"
      | Dishonest ->
          Format.pp_print_string fmtr "Dishonest"

    let encoding : t Data_encoding.t =
      let open Data_encoding in
      union
        [ case
            ~title:"Honest"
            (Tag 0)
            unit
            (function Honest -> Some () | _ -> None)
            (fun () -> Honest);
          case
            ~title:"Dishonest"
            (Tag 1)
            unit
            (function Dishonest -> Some () | _ -> None)
            (fun () -> Dishonest) ]

    let health = function
      | Honest ->
          honest_health
      | Dishonest ->
          dishonest_health

    let share = function
      | Honest ->
          honest_share
      | Dishonest ->
          dishonest_share
  end : Player.S
    with type t = players )

module Instantiate_all (X : sig
  module Proto : Protocol.S

  val undelegated : float

  val honest_health : float

  val dishonest_health : float

  val honest_share : float

  val dishonest_share : float

  val nsamples : int

  val adversary : adversary

  val debug_log : bool
end) =
struct
  module P =
  ( val instantiate_players
          ~honest_health:X.honest_health
          ~dishonest_health:X.dishonest_health
          ~honest_share:X.honest_share
          ~dishonest_share:X.dishonest_share )

  module Helpers = Player.Make_helpers (P)
  module Proto = X.Proto

  module Simu =
    Simulator.Make (Proto) (P) (Helpers)
      (struct
        let max_delay_before_chain_death = 120.

        let bakers_priority_list_length = 10

        let debug_log = X.debug_log
      end)

  module Baking = Simu.Game

  (* find lowest priority head *)
  let lowest_priority : Baking.heads -> Baking.block =
    let rec loop heads best =
      match heads with
      | [] ->
          best
      | head :: tl ->
          if head.Baking.outcome.slot < best.Baking.outcome.slot then
            loop tl head
          else loop tl best
    in
    fun heads ->
      match heads with [] -> assert false | head :: tl -> loop tl head

  (* find lowest age head *)
  let lowest_age : Baking.heads -> Baking.block =
    let rec loop heads best =
      match heads with
      | [] ->
          best
      | head :: tl ->
          if head.Baking.age < best.Baking.age then loop tl head
          else loop tl best
    in
    fun heads ->
      match heads with [] -> assert false | head :: tl -> loop tl head

  (* Endorse head *)
  let endorse_head :
      player_endos:int -> Baking.block -> Baking.endorsement_move =
   fun ~player_endos best_head ->
    if player_endos = 0 then Baking.Diffuse_endorsements None
    else
      Baking.Diffuse_endorsements
        (Some (player_endos, best_head.Baking.block_uid))

  let collect_published_endorsements_for_head target_head mempool =
    let open Helpers in
    Player_map.fold
      (fun player {Baking.published_endorsements = diffuse_opt; _} acc ->
        match diffuse_opt with
        | None ->
            acc
        | Some (count, block_id) ->
            if Block_id.equal block_id target_head then
              Int_vec.(add acc (basis player count))
            else acc)
      mempool.Baking.mempools
      Int_vec.zero

  let honest_baking_strategy :
      players ->
      Baking.level ->
      Baking.block ->
      Baking.strategy ->
      Baking.baking_strategy =
    let open Helpers in
    let open M in
    fun player level best_head k mempool ->
      let used_public_endorsements =
        collect_published_endorsements_for_head best_head.block_uid mempool
      in
      match Int_vec.get player level.block_priority with
      | 0 ->
          debug "honest player: priority 0, player bakes@." ;
          return
            ( Baking.Bake
                {used_public_endorsements; on_top_of = best_head.block_uid},
              k )
      | prio when prio < max_int ->
          debug "honest player: priority %d@." prio ;
          return (Baking.Do_not_bake, k)
      | _ ->
          debug "honest player: priority = infinity@." ;
          return (Baking.Do_not_bake, k)

  let rec cooperative_strategy : players -> Baking.strategy =
    let open Helpers in
    let open M in
    fun player ->
      Strategy
        (fun ~height ~levels ~heads ->
          let level = levels height in
          let player_endos = Int_vec.get player level.endorsers in
          let best_head = lowest_age heads in
          return
            ( endorse_head ~player_endos best_head,
              honest_baking_strategy
                player
                level
                best_head
                (cooperative_strategy player) ))

  module Selfish = struct
    module Constants = struct
      let standard_block_delay =
        Proto.minimal_valid_time
          ~priority:0
          ~endorsing_power:Proto.endorsers_per_block

      let total_supply = 800_000_000.0
    end

    type baker_state = {
      stake : float;
      (* rewards per second estimates *)
      rps : Helpers.Float_vec.t;
    }

    type reward_vec = {baker : float; non_baker : float}

    let delay_of_level (level : Baking.level) : float =
      Proto.minimal_valid_time
        ~priority:0
        ~endorsing_power:(Helpers.Int_vec.total level.endorsers)

    let discounted_total_rewards ~level ~baker ~non_baker ~bs ~priority
        ~baker_endos ~non_baker_endos =
      let open Helpers in
      let included_endorsements = baker_endos + non_baker_endos in
      let block_delay =
        Proto.minimal_valid_time
          ~priority
          ~endorsing_power:included_endorsements
      in
      let delta_t = block_delay -. delay_of_level level in
      let discounted_baker_rewards =
        Proto.block_reward ~priority ~included_endorsements
        +. Proto.endorsing_reward ~priority ~num_slots:baker_endos
        -. (Float_vec.get baker bs.rps *. delta_t)
      in
      let discounted_non_baker_rewards =
        Proto.endorsing_reward ~priority ~num_slots:non_baker_endos
        -. (Float_vec.get non_baker bs.rps *. delta_t)
      in
      {
        baker = discounted_baker_rewards;
        non_baker = discounted_non_baker_rewards;
      }

    let update_baker_state (bs : baker_state)
        ({age; rewards; _} : Baking.block) =
      let open Helpers in
      let rps = Float_vec.map (fun x -> x /. age) rewards in
      {bs with rps}

    let compute_steal_induced_delay priority (level : Baking.level) =
      let open Helpers in
      let honest_endos = Int_vec.get Honest level.endorsers in
      let dishonest_endos = Int_vec.get Dishonest level.endorsers in
      let delay_honest =
        Proto.minimal_valid_time ~priority:0 ~endorsing_power:honest_endos
      in
      let delay_dishonest =
        Proto.minimal_valid_time
          ~priority
          ~endorsing_power:(honest_endos + dishonest_endos)
      in
      (delay_honest, delay_dishonest)

    let compute_steal_induced_reward priority level bs =
      let open Helpers in
      let honest_endos = Int_vec.get Honest level.Baking.endorsers in
      let dishonest_endos = Int_vec.get Dishonest level.Baking.endorsers in
      let steal_rewards =
        discounted_total_rewards
          ~level
          ~baker:Dishonest
          ~non_baker:Honest
          ~bs
          ~priority
          ~baker_endos:dishonest_endos
          ~non_baker_endos:honest_endos
      in
      let cooperate_rewards =
        Proto.endorsing_reward ~priority:0 ~num_slots:dishonest_endos
      in
      (cooperate_rewards, steal_rewards.baker)

    let is_simple_steal_feasible ~priority ~level =
      let (delay_honest, delay_dishonest) =
        compute_steal_induced_delay priority level
      in
      let () =
        debug
          "delay_dishonest=%f vs delay_honest=%f@."
          delay_dishonest
          delay_honest
      in
      delay_honest > delay_dishonest

    let is_simple_steal_profitable ~priority ~level block_state =
      let (reward_cooperate, reward_steal) =
        compute_steal_induced_reward priority level block_state
      in
      reward_cooperate < reward_steal

    let compute_two_level_steal_induced_rewards priority height levels bs =
      let open Helpers in
      let level = levels height in
      let next_level = levels (height + 1) in
      let prio_dishonest_n =
        Int_vec.get Dishonest level.Baking.block_priority
      in
      let prio_honest_succ_n = Int_vec.get Honest next_level.block_priority in
      assert (prio_dishonest_n > 0) ;
      assert (prio_honest_succ_n > 0) ;
      let honest_endos = Int_vec.get Honest level.endorsers in
      let next_honest_endos = Int_vec.get Honest next_level.endorsers in
      let dishonest_endos = Int_vec.get Dishonest level.endorsers in
      let next_dishonest_endos = Int_vec.get Dishonest next_level.endorsers in
      let dishonest_reward_cooperate_path =
        let endo_rewards =
          Proto.endorsing_reward ~priority:0 ~num_slots:dishonest_endos
        in
        let baking_rewards =
          discounted_total_rewards
            ~level:next_level
            ~baker:Dishonest
            ~non_baker:Honest
            ~bs
            ~priority:0
            ~baker_endos:next_dishonest_endos
            ~non_baker_endos:next_honest_endos
        in
        endo_rewards +. baking_rewards.baker
      in
      let dishonest_reward_steal_path =
        let stolen_block_rewards =
          discounted_total_rewards
            ~level
            ~baker:Dishonest
            ~non_baker:Honest
            ~bs
            ~priority
            ~baker_endos:dishonest_endos
            ~non_baker_endos:honest_endos
        in
        let prio0_block_rewards =
          discounted_total_rewards
            ~level:next_level
            ~baker:Dishonest
            ~non_baker:Honest
            ~bs
            ~priority:0
            ~baker_endos:next_dishonest_endos
            ~non_baker_endos:0
        in
        stolen_block_rewards.baker +. prio0_block_rewards.baker
      in
      (dishonest_reward_cooperate_path, dishonest_reward_steal_path)

    let compute_two_level_steal_induced_delay height levels =
      let open Helpers in
      let level = levels height in
      let prio_dishonest_n =
        Int_vec.get Dishonest level.Baking.block_priority
      in
      let next_level = levels (height + 1) in
      let prio_honest_succ_n = Int_vec.get Honest next_level.block_priority in
      assert (prio_dishonest_n > 0) ;
      assert (prio_honest_succ_n > 0) ;
      let honest_endos = Int_vec.get Honest level.endorsers in
      let next_honest_endos = Int_vec.get Honest next_level.endorsers in
      let dishonest_endos = Int_vec.get Dishonest level.endorsers in
      let next_dishonest_endos = Int_vec.get Dishonest next_level.endorsers in
      let delay_honest_branch =
        Proto.minimal_valid_time ~priority:0 ~endorsing_power:honest_endos
        +. Proto.minimal_valid_time
             ~priority:prio_honest_succ_n
             ~endorsing_power:next_honest_endos
      in
      let delay_dishonest_branch =
        Proto.minimal_valid_time
          ~priority:prio_dishonest_n
          ~endorsing_power:(honest_endos + dishonest_endos)
        +. Proto.minimal_valid_time
             ~priority:0
             ~endorsing_power:next_dishonest_endos
      in
      (delay_honest_branch, delay_dishonest_branch)

    let is_two_level_steal_feasible height levels =
      let (delay_honest_branch, delay_dishonest_branch) =
        compute_two_level_steal_induced_delay height levels
      in
      delay_dishonest_branch < delay_honest_branch

    let is_two_level_steal_profitable priority height levels baker_state =
      let (rewards_honest_branch, rewards_dishonest_branch) =
        compute_two_level_steal_induced_rewards
          priority
          height
          levels
          baker_state
      in
      rewards_dishonest_branch > rewards_honest_branch

    let bake_selfish target_head k =
      let open Baking in
      let open M in
      return
        ( Diffuse_endorsements None,
          fun mempool ->
            let used_public_endorsements =
              collect_published_endorsements_for_head target_head mempool
            in
            debug "selfish player bakes@." ;
            return (Bake {used_public_endorsements; on_top_of = target_head}, k)
        )

    let cooperate level target_head k =
      let open Helpers in
      let open M in
      let dishonest_endos = Int_vec.get Dishonest level.Baking.endorsers in
      return
        ( Baking.Diffuse_endorsements (Some (dishonest_endos, target_head)),
          fun _mempool -> return (Baking.Do_not_bake, k) )

    let rec selfish_strategy : Baking.strategy =
      let open Helpers in
      Strategy
        (fun ~height ~levels ~heads ->
          let level = levels height in
          let best_head = lowest_age heads in
          let priority = Int_vec.get Dishonest level.block_priority in
          debug "selfish player: priority = %d@." priority ;
          match priority with
          | 0 ->
              let player_endos = Int_vec.get Dishonest level.endorsers in
              M.return
                ( endorse_head ~player_endos best_head,
                  fun mempool ->
                    let used_public_endorsements =
                      collect_published_endorsements_for_head
                        best_head.block_uid
                        mempool
                    in
                    M.return
                      ( Baking.Bake
                          {
                            used_public_endorsements;
                            on_top_of = best_head.block_uid;
                          },
                        selfish_strategy ) )
          (* cooperative_strategy
           *   bake_selfish best_head.block_uid selfish_strategy *)
          | _ ->
              if is_simple_steal_feasible ~priority ~level then
                (* try to steal *)
                bake_selfish best_head.block_uid selfish_strategy
              else cooperate level best_head.block_uid selfish_strategy)

    let rec rational_selfish_strategy bs : Baking.strategy =
      let open Helpers in
      Strategy
        (fun ~height ~levels ~heads ->
          let level = levels height in
          let best_head = lowest_age heads in
          let bs = update_baker_state bs best_head in
          let priority = Int_vec.get Dishonest level.block_priority in
          debug "selfish player: priority = %d@." priority ;
          match priority with
          | priority when priority = max_int ->
              cooperate
                level
                best_head.block_uid
                (rational_selfish_strategy bs)
          | 0 ->
              bake_selfish best_head.block_uid (rational_selfish_strategy bs)
          | _ ->
              if
                is_simple_steal_feasible ~priority ~level
                && is_simple_steal_profitable ~priority ~level bs
              then
                (* try to steal *)
                bake_selfish best_head.block_uid (rational_selfish_strategy bs)
              else
                cooperate
                  level
                  best_head.block_uid
                  (rational_selfish_strategy bs))

    let ar_attacker baker_state : Baking.strategy =
      let open Helpers in
      let rec scanning_for_blocks bs : Baking.strategy =
        Strategy
          (fun ~height ~levels ~heads ->
            let level = levels height in
            let next_level = levels (height + 1) in
            let best_head = lowest_age heads in
            let bs = update_baker_state bs best_head in
            let priority = Int_vec.get Dishonest level.block_priority in
            let next_priority =
              Int_vec.get Dishonest next_level.block_priority
            in
            debug "selfish player: priority = %d@." priority ;
            match (priority, next_priority) with
            | (priority, _) when priority = max_int ->
                cooperate level best_head.block_uid (scanning_for_blocks bs)
            | (0, _) ->
                bake_selfish best_head.block_uid (scanning_for_blocks bs)
            | (_, 0) ->
                if
                  is_two_level_steal_feasible height levels
                  && is_two_level_steal_profitable priority height levels bs
                then bake_selfish best_head.block_uid (perform_steal bs)
                else
                  cooperate level best_head.block_uid (scanning_for_blocks bs)
            | (_, _) ->
                if
                  is_simple_steal_feasible ~priority ~level
                  && is_simple_steal_profitable ~priority ~level bs
                then
                  (* try to steal *)
                  bake_selfish best_head.block_uid (scanning_for_blocks bs)
                else
                  cooperate level best_head.block_uid (scanning_for_blocks bs))
      and perform_steal bs : Baking.strategy =
        Strategy
          (fun ~height ~levels ~heads ->
            let level = levels height in
            let priority = Int_vec.get Dishonest level.block_priority in
            assert (priority = 0) ;
            let stolen_block =
              match
                List.find_opt
                  (fun block ->
                    P.compare block.Baking.outcome.baker Dishonest = 0)
                  heads
              with
              | None ->
                  Format.eprintf
                    "at height %d: could not find selfish block (%d heads)@."
                    height
                    (List.length heads) ;
                  assert false
              | Some res ->
                  res
            in
            bake_selfish stolen_block.Baking.block_uid (scanning_for_blocks bs))
      in
      scanning_for_blocks baker_state

    let find_min predicate max_iter =
      let rec loop i =
        if i > max_iter then None
        else if predicate i then Some i
        else loop (i + 1)
      in
      loop 0

    (* only to be called when dishonest_priority = 0 *)
    let optimize_public_endorsements_for_delay honest_endos dishonest_endos
        level =
      let open Baking in
      let open Helpers in
      let honest_priority = Int_vec.get Honest level.block_priority in
      assert (honest_priority > 0) ;
      let honest_delay =
        Proto.minimal_valid_time
          ~priority:honest_priority
          ~endorsing_power:honest_endos
      in
      find_min
        (fun num_endos ->
          Proto.minimal_valid_time
            ~priority:0
            ~endorsing_power:(dishonest_endos + num_endos)
          < honest_delay)
        honest_endos

    (* only to be called when dishonest_priority = 0 *)
    let bake_deflationary ?(optimize_for_efficiency = fun i _ -> i) level
        target_head k =
      let open Baking in
      let open Helpers in
      let open M in
      return
        ( Diffuse_endorsements None,
          fun mempool ->
            let honest_endos =
              let map =
                Block_id.Map.find target_head mempool.public_endorsements
              in
              Int_vec.get Honest map
            in
            let dishonest_endos = Int_vec.get Dishonest level.endorsers in
            let used_honest_endos =
              match
                optimize_public_endorsements_for_delay
                  honest_endos
                  dishonest_endos
                  level
              with
              | None ->
                  assert false
              | Some i ->
                  optimize_for_efficiency i honest_endos
            in
            let used_public_endorsements =
              Int_vec.basis Honest used_honest_endos
            in
            debug "selfish player bakes@." ;
            return (Bake {used_public_endorsements; on_top_of = target_head}, k)
        )

    let rec deflationary_simple : Baking.strategy =
      let open Helpers in
      Strategy
        (fun ~height ~levels ~heads ->
          let level = levels height in
          let best_head = lowest_age heads in
          let priority = Int_vec.get Dishonest level.block_priority in
          match priority with
          | 0 ->
              bake_deflationary level best_head.block_uid deflationary_simple
          | _ ->
              cooperate level best_head.block_uid deflationary_simple)

    (* Try to find the minimum amount of honest endorsement to include in
       order to be able to steal a block when dishonest is in priority > 0 *)
    let optimize_steal_induced_delay priority (level : Baking.level) =
      let open Helpers in
      let honest_endos = Int_vec.get Honest level.endorsers in
      let dishonest_endos = Int_vec.get Dishonest level.endorsers in
      let honest_delay =
        Proto.minimal_valid_time ~priority:0 ~endorsing_power:honest_endos
      in
      find_min
        (fun num_endos ->
          Proto.minimal_valid_time
            ~priority
            ~endorsing_power:(dishonest_endos + num_endos)
          < honest_delay)
        honest_endos

    let bake_with_prescribed_amount public_endos target_head k =
      let open Baking in
      let open Helpers in
      let open M in
      return
        ( Diffuse_endorsements None,
          fun _mempool ->
            debug "selfish player bakes@." ;
            let used_public_endorsements = Int_vec.basis Honest public_endos in
            return (Bake {used_public_endorsements; on_top_of = target_head}, k)
        )

    let iar s_a r_a r_b supply =
      (r_a -. (s_a *. (r_a +. r_b))) /. (s_a *. (supply +. r_a +. r_b))

    let optimize_iar baker_state (level : Baking.level) (head : Baking.block)
        priority min_endos max_endos =
      let open Helpers in
      let found = ref min_endos in
      let max_iar = ref ~-.max_float in
      let dishonest_endos = Int_vec.get Dishonest level.endorsers in
      let total_dishonest_rewards = Float_vec.get Dishonest head.rewards in
      let total_honest_rewards = Float_vec.get Honest head.rewards in
      for i = min_endos to max_endos do
        let rewards_vec =
          discounted_total_rewards
            ~level
            ~baker:Dishonest
            ~non_baker:Honest
            ~bs:baker_state
            ~priority
            ~baker_endos:dishonest_endos
            ~non_baker_endos:i
        in
        let r_me = rewards_vec.baker +. total_dishonest_rewards in
        let r_him = rewards_vec.non_baker +. total_honest_rewards in
        let iar_value =
          iar baker_state.stake r_me r_him Constants.total_supply
        in
        if iar_value > !max_iar then (
          max_iar := iar_value ;
          found := i )
      done ;
      (!found, !max_iar)

    let optimize_iar' bs l h prio min max =
      fst (optimize_iar bs l h prio min max)

    let optimize_rsd baker_state (level : Baking.level) (head : Baking.block)
        priority min_endos max_endos =
      let open Helpers in
      let his_stake = 1. -. baker_state.stake in
      let found = ref min_endos in
      let max_rsd = ref ~-.max_float in
      let dishonest_endos = Int_vec.get Dishonest level.endorsers in
      let total_dishonest_rewards = Float_vec.get Dishonest head.rewards in
      let total_honest_rewards = Float_vec.get Honest head.rewards in
      for i = min_endos to max_endos do
        let rewards_vec =
          discounted_total_rewards
            ~level
            ~baker:Dishonest
            ~non_baker:Honest
            ~bs:baker_state
            ~priority
            ~baker_endos:dishonest_endos
            ~non_baker_endos:i
        in
        let r_me = rewards_vec.baker +. total_dishonest_rewards in
        let r_him = rewards_vec.non_baker +. total_honest_rewards in
        let rsd_value = (r_me /. baker_state.stake) -. (r_him /. his_stake) in
        if rsd_value > !max_rsd then (
          max_rsd := rsd_value ;
          found := i )
      done ;
      (!found, !max_rsd)

    let optimize_rsd' bs l h prio min max =
      fst (optimize_rsd bs l h prio min max)

    let rec iar_attacker baker_state : Baking.strategy =
      let open Helpers in
      Strategy
        (fun ~height ~levels ~heads ->
          let level = levels height in
          let best_head = lowest_age heads in
          let baker_state = update_baker_state baker_state best_head in
          let priority = Int_vec.get Dishonest level.block_priority in
          match priority with
          | 0 ->
              bake_deflationary
                ~optimize_for_efficiency:
                  (optimize_iar' baker_state level best_head 0)
                level
                best_head.block_uid
                (iar_attacker baker_state)
          | _ when priority < max_int -> (
            match optimize_steal_induced_delay priority level with
            | None ->
                cooperate level best_head.block_uid (iar_attacker baker_state)
            | Some min_public_endos ->
                (* Should we cooperate or bake?
                   Compute optimal # of endorsements to include if we steal *)
                let (endos_to_include_in_steal, steal_iar) =
                  let max_endos = Int_vec.get Honest level.endorsers in
                  optimize_iar
                    baker_state
                    level
                    best_head
                    priority
                    min_public_endos
                    max_endos
                in
                let coop_iar =
                  let h_endos = Int_vec.get Honest level.endorsers in
                  let d_endos = Int_vec.get Dishonest level.endorsers in
                  let rew_vec =
                    discounted_total_rewards
                      ~level
                      ~baker:Honest
                      ~non_baker:Dishonest
                      ~bs:baker_state
                      ~priority:0
                      ~baker_endos:h_endos
                      ~non_baker_endos:d_endos
                  in
                  let total_h_r =
                    rew_vec.baker +. Float_vec.get Honest best_head.rewards
                  in
                  let total_d_r =
                    rew_vec.non_baker
                    +. Float_vec.get Dishonest best_head.rewards
                  in
                  iar
                    baker_state.stake
                    total_d_r
                    total_h_r
                    Constants.total_supply
                in
                if steal_iar > coop_iar then
                  bake_with_prescribed_amount
                    endos_to_include_in_steal
                    best_head.block_uid
                    (iar_attacker baker_state)
                else
                  cooperate
                    level
                    best_head.block_uid
                    (iar_attacker baker_state) )
          | _ ->
              cooperate level best_head.block_uid (iar_attacker baker_state))

    let rec rsd_attacker baker_state : Baking.strategy =
      let open Helpers in
      Strategy
        (fun ~height ~levels ~heads ->
          let level = levels height in
          let best_head = lowest_age heads in
          let baker_state = update_baker_state baker_state best_head in
          let priority = Int_vec.get Dishonest level.block_priority in
          match priority with
          | 0 ->
              (* deflationary baking *)
              bake_deflationary
                ~optimize_for_efficiency:
                  (optimize_rsd' baker_state level best_head 0)
                level
                best_head.block_uid
                (rsd_attacker baker_state)
          | _ when priority < max_int -> (
            match optimize_steal_induced_delay priority level with
            | None ->
                cooperate level best_head.block_uid (rsd_attacker baker_state)
            | Some min_public_endos ->
                let (endos_to_include, steal_rsd) =
                  let max_endos = Int_vec.get Honest level.endorsers in
                  optimize_rsd
                    baker_state
                    level
                    best_head
                    priority
                    min_public_endos
                    max_endos
                in
                let coop_rsd =
                  let h_endos = Int_vec.get Honest level.endorsers in
                  let d_endos = Int_vec.get Dishonest level.endorsers in
                  let rew_vec =
                    discounted_total_rewards
                      ~level
                      ~baker:Honest
                      ~non_baker:Dishonest
                      ~bs:baker_state
                      ~priority:0
                      ~baker_endos:h_endos
                      ~non_baker_endos:d_endos
                  in
                  let total_h_r =
                    rew_vec.baker +. Float_vec.get Honest best_head.rewards
                  in
                  let total_d_r =
                    rew_vec.non_baker
                    +. Float_vec.get Dishonest best_head.rewards
                  in
                  (total_d_r /. baker_state.stake)
                  -. (total_h_r /. (1. -. baker_state.stake))
                in
                if steal_rsd > coop_rsd then
                  bake_with_prescribed_amount
                    endos_to_include
                    best_head.block_uid
                    (rsd_attacker baker_state)
                else
                  cooperate
                    level
                    best_head.block_uid
                    (rsd_attacker baker_state) )
          | _ ->
              cooperate level best_head.block_uid (rsd_attacker baker_state))
  end

  let arena =
    let open Helpers in
    Player_map.mapi
      (fun player () ->
        match player with
        | Honest ->
            cooperative_strategy Honest
        | Dishonest -> (
            let normalized_dishonest_share =
              X.dishonest_share /. (1. -. X.undelegated)
            in
            let rps = Float_vec.zero in
            let baker_state : Selfish.baker_state =
              {stake = normalized_dishonest_share; rps}
            in
            match X.adversary with
            | Not_dishonest ->
                cooperative_strategy Dishonest
            | Naive_block_stealing ->
                Selfish.selfish_strategy
            | AR_one_step ->
                Selfish.rational_selfish_strategy baker_state
            | AR_two_step ->
                Selfish.ar_attacker baker_state
            | Deflationary ->
                Selfish.deflationary_simple
            | IAR_attacker ->
                Selfish.iar_attacker baker_state
            | RSD_attacker ->
                let normalized_dishonest_share =
                  X.dishonest_share /. (1. -. X.undelegated)
                in
                let rps = Float_vec.zero in
                let baker_state : Selfish.baker_state =
                  {stake = normalized_dishonest_share; rps}
                in
                Selfish.rsd_attacker baker_state ))
      Simu.players_map

  let youngest_after_n_rounds n = Simu.simulate arena (Simu.height_predicate n)

  let youngest_after_time_t t = Simu.simulate arena (Simu.age_predicate t)

  (* The statistics we are interested in *)

  let honest_rewards (chain : Simu.chain) =
    let head = Simu.chain_head_exn chain in
    Helpers.Float_vec.get Honest head.rewards

  let dishonest_rewards (chain : Simu.chain) =
    let head = Simu.chain_head_exn chain in
    Helpers.Float_vec.get Dishonest head.rewards

  let shares_of_total_rewards (chain : Simu.chain) =
    let head = Simu.chain_head_exn chain in
    let h = Helpers.Float_vec.get Honest head.rewards in
    let d = Helpers.Float_vec.get Dishonest head.rewards in
    let max_rewards =
      let h = float head.fitness in
      h *. Proto.total_rewards_per_block
    in
    (* Format.eprintf "max_rewards: %f, actual rewards: %f@." max_rewards (h +. d) ; *)
    {max_rewards; total_hon = h; total_dis = d}

  let steal_proba (chain : Simu.chain) =
    let steals =
      List.fold_left
        (fun acc block ->
          if block.Baking.outcome.slot > 0 then acc + 1 else acc)
        0
        chain
    in
    let head = Simu.chain_head_exn chain in
    float steals /. float head.fitness

  let stolen_rewards (chain : Simu.chain) =
    let open Helpers in
    List.fold_left
      (fun acc block ->
        if block.Baking.outcome.slot > 0 then
          let dishonest_rewards =
            block.outcome.block_reward
            +. Float_vec.get Dishonest block.outcome.endorsement_rewards
          in
          let honest_rewards =
            let num_slots = Int_vec.get Dishonest block.level.endorsers in
            Proto.endorsing_reward ~priority:0 ~num_slots
          in
          let delta = dishonest_rewards -. honest_rewards in
          acc +. delta
        else acc)
      0.0
      chain

  let shave_overdue time (chain : Simu.chain) =
    match chain with
    | [] ->
        assert false
    | head :: tail ->
        let time_overdue = head.age -. time in
        let block_duration = head.outcome.delay in
        let shrink_ratio = 1. -. (time_overdue /. block_duration) in
        let shaved_outcome =
          {
            head.outcome with
            (* delay = head.outcome.delay -. time_overdue *)
            block_reward = head.outcome.block_reward *. shrink_ratio;
            endorsement_rewards =
              Helpers.Float_vec.smul
                shrink_ratio
                head.outcome.endorsement_rewards;
          }
        in
        let head = Simu.make_head shaved_outcome head.level (List.hd tail) in
        head :: tail

  let rewards_ratio_proba ~time =
    M.map
      (fun chain ->
        let chain = shave_overdue time chain in
        let length = float (List.length chain) in
        let rewards = shares_of_total_rewards chain in
        let steal_proba = steal_proba chain in
        let stolen_rewards = stolen_rewards chain in
        {rewards; stolen_rewards; steal_proba; length})
      (youngest_after_time_t time)

  let rewards_ratio_proba_n ~length =
    M.map
      (fun chain ->
        let length = float (List.length chain) in
        let rewards = shares_of_total_rewards chain in
        let steal_proba = steal_proba chain in
        let stolen_rewards = stolen_rewards chain in
        {rewards; stolen_rewards; steal_proba; length})
      (youngest_after_n_rounds length)

  let simulation ~time =
    Stats.empirical_of_generative
      ~nsamples:X.nsamples
      (rewards_ratio_proba ~time)

  let simulation_length ~length =
    Stats.empirical_of_generative
      ~nsamples:X.nsamples
      (rewards_ratio_proba_n ~length)
end

(* Actual simulations *)

(* We simulate n_hours hours worth of chain *)
let n_hours = 8.

let simulation_time = 60.0 *. 60.0 *. n_hours

type processed_simu_results = {
  total_h : float;
  total_d : float;
  stolen_d : float;
  relative_h : float;
  relative_d : float;
  max_rew : float;
  honest_share : float;
  dishonest_share : float;
  steal : float;
  length : float;
}

type rgb = {r : float; g : float; b : float}

let make_gradient =
  let color_gradient =
    let remove_last l =
      match l with [] -> [] | _ -> List.rev (List.tl (List.rev l))
    in
    let open Pyplot.Plot in
    let interpolate c1 c2 al =
      let ba = 1. -. al in
      let {r = r1; g = g1; b = b1} = c1 in
      let {r = r2; g = g2; b = b2} = c2 in
      Rgb
        {
          r = (ba *. r1) +. (al *. r2);
          g = (ba *. g1) +. (al *. g2);
          b = (ba *. b1) +. (al *. b2);
        }
    in
    let points =
      Numerics.Float64.Vec.(to_array (linspace 0.0 1.0 3)) |> Array.to_list
    in
    let c0 = {r = 1.0; g = 0.0; b = 0.0} in
    let c1 = {r = 0.0; g = 0.0; b = 1.0} in
    let c2 = {r = 0.0; g = 1.0; b = 0.0} in
    remove_last (List.map (interpolate c0 c1) points)
    @ List.map (interpolate c1 c2) points
    |> List.sort compare |> Array.of_list
  in
  fun () ->
    let x = ref 0 in
    fun () ->
      let clr = color_gradient.(!x) in
      x := (!x + 1) mod Array.length color_gradient ;
      clr

let float_list_to_matrix (l : float list) =
  let arr = Array.of_list l in
  Pyplot.Matrix.init ~lines:(Array.length arr) ~cols:1 ~f:(fun l _ -> arr.(l))

let dot_style_of_adversary color (adversary : adversary) =
  let open Pyplot.Plot in
  match adversary with
  | Not_dishonest ->
      {linestyle = Line_solid; dot_style = None; color = Blue}
  | IAR_attacker ->
      {
        linestyle = Line_dashed;
        dot_style = None;
        color = Rgb {r = 50. /. 255.; g = 200. /. 255.; b = 50. /. 255.};
      }
  | AR_two_step ->
      {linestyle = Line_dashdot; dot_style = None; color = Red}
  | RSD_attacker ->
      {
        linestyle = Line_dotted;
        dot_style = None;
        color = Rgb {r = 0.0; g = 0.0; b = 0.0};
      }
  | Deflationary | Naive_block_stealing | AR_one_step ->
      {linestyle = Line_solid; dot_style = None; color}

(* ------------------------------------- *)
(* ------------------------------------- *)
(* Entrypoint *)

module Protocol_map = Map.Make (struct
  type t = protocol

  let compare = Stdlib.compare
end)

module Adversary_map = Map.Make (struct
  type t = adversary

  let compare = Stdlib.compare
end)

let protocol_plot :
    indicator ->
    protocol ->
    (float * float) list Adversary_map.t ->
    (unit, 'a) Pyplot.Plot.Axis.t =
 fun indicator protocol data ->
  let open Pyplot.Plot.Axis in
  let genclr = make_gradient () in
  let lines =
    Adversary_map.fold
      (fun adversary f cmd ->
        let (xs, ys) = List.split f in
        let xs = float_list_to_matrix xs in
        let ys = float_list_to_matrix ys in
        let legend = Some (Format.asprintf "%a" pp_adversary adversary) in
        let color = genclr () in
        let line = Some (dot_style_of_adversary color adversary) in
        cmd >>= fun () -> line_2d ~xs ~ys ~legend ~line)
      data
      (return ())
  in
  lines
  >>= fun () ->
  set_xlabel "non-cooperative stake"
  >>= fun () ->
  set_ylim ~ymin:(Some 0.0) ~ymax:None
  >>= fun () ->
  let indicator_s = Format.asprintf "%a" pp_indicator indicator in
  set_ylabel indicator_s
  >>= fun () ->
  let title =
    Format.asprintf "%a / %a" pp_indicator indicator pp_protocol protocol
  in
  set_title title >>= fun () -> legend ()

let generic_plot :
    bool ->
    indicator ->
    (float * float) list Adversary_map.t Protocol_map.t ->
    unit =
 fun show_flag indicator protocol_map ->
  let total_card = Protocol_map.cardinal protocol_map in
  let ncols = if total_card <= 1 then 1 else 2 in
  let nrows =
    if total_card mod 2 = 0 then total_card / 2 else (total_card / 2) + 1
  in
  let pos index =
    if index mod 2 = 0 then
      let col = 0 in
      let row = index / 2 in
      (row, col)
    else
      let col = 1 in
      let row = index / 2 in
      (row, col)
  in
  let open Pyplot.Plot in
  init () ;
  run
    ~nrows
    ~ncols
    (let (_, plots) =
       Protocol_map.fold
         (fun protocol advs (index, acc) ->
           let (row, col) = pos index in
           let axis = protocol_plot indicator protocol advs in
           (index + 1, acc >>= fun () -> subplot_2d ~row ~col axis))
         protocol_map
         (0, return ())
     in
     plots
     >>= fun () ->
     let title =
       Format.asprintf "Comparative %a plot" pp_indicator indicator
     in
     suptitle ~title ~fontsize:(Some 16)
     >>= fun () ->
     if show_flag then show ()
     else
       return ()
       >>= fun () ->
       let filename = Format.asprintf "%a.png" pp_indicator indicator in
       savefig ~filename ~dpi:300 ~quality:95)

let filename_from_parameters
    { adversary;
      protocol;
      undelegated;
      dishonest_health;
      honest_health;
      seed;
      nsamples = _;
      debug = _ } =
  Format.asprintf
    "%a-%a-dh=%.1f-hh=%.1f-u=%.1f-%a.trace"
    pp_adversary
    adversary
    pp_protocol
    protocol
    dishonest_health
    honest_health
    undelegated
    (Format.pp_print_option
       ~none:(fun fmtr () -> Format.fprintf fmtr "selfinit")
       Format.pp_print_int)
    seed

let arrange_by_protocol :
    savedata list -> simulation_results list Adversary_map.t Protocol_map.t =
 fun savedata_list ->
  let used_protocols =
    List.fold_left
      (fun acc (sampling_params, _) ->
        let p = sampling_params.protocol in
        if List.mem p acc then acc else p :: acc)
      []
      savedata_list
  in
  let map =
    (* Populate map *)
    List.fold_left
      (fun map protocol -> Protocol_map.add protocol Adversary_map.empty map)
      Protocol_map.empty
      used_protocols
  in
  List.fold_left
    (fun pmap (params, data) ->
      let advs = Protocol_map.find params.protocol pmap in
      let resl = Adversary_map.find_opt params.adversary advs in
      let resl =
        match resl with
        | None ->
            data
        | Some _ ->
            let msg =
              Format.asprintf
                "Multiple trace files for the (%a, %a)"
                pp_protocol
                params.protocol
                pp_adversary
                params.adversary
            in
            failwith msg
      in
      let advs = Adversary_map.add params.adversary resl advs in
      Protocol_map.add params.protocol advs pmap)
    map
    savedata_list

let process_empirical_data :
    simulation_results list -> processed_simu_results list =
 fun list ->
  List.map
    (fun {empirical; honest_share; dishonest_share} ->
      let rewards = Stats.map_emp (fun {rewards; _} -> rewards) empirical in
      let steal =
        Stats.map_emp (fun {steal_proba; _} -> steal_proba) empirical
        |> Stats.mean (module Structures.Float)
      in
      let length =
        Stats.map_emp (fun (x : chain_statistics) -> x.length) empirical
        |> Stats.mean (module Structures.Float)
      in
      let total_h =
        Stats.map_emp (fun {total_hon; _} -> total_hon) rewards
        |> Stats.mean (module Structures.Float)
      in
      let total_d =
        Stats.map_emp (fun {total_dis; _} -> total_dis) rewards
        |> Stats.mean (module Structures.Float)
      in
      let stolen_d =
        Stats.map_emp (fun x -> x.stolen_rewards) empirical
        |> Stats.mean (module Structures.Float)
      in
      let relative_h =
        Stats.map_emp
          (fun {total_hon; total_dis; _} ->
            total_hon /. (total_hon +. total_dis))
          rewards
        |> Stats.mean (module Structures.Float)
      in
      let relative_d =
        Stats.map_emp
          (fun {total_hon; total_dis; _} ->
            total_dis /. (total_hon +. total_dis))
          rewards
        |> Stats.mean (module Structures.Float)
      in
      let max_rewards =
        Stats.map_emp (fun {max_rewards; _} -> max_rewards) rewards
        |> Stats.mean (module Structures.Float)
      in
      {
        total_h;
        total_d;
        stolen_d;
        relative_h;
        relative_d;
        max_rew = max_rewards;
        honest_share;
        dishonest_share;
        steal;
        length;
      })
    list

let ar_indicator : processed_simu_results -> float =
 fun results -> results.total_d

let iar s_a r_a r_b supply =
  (r_a -. (s_a *. (r_a +. r_b))) /. (s_a *. (supply +. r_a +. r_b))

let iar_indicator : processed_simu_results -> float =
 fun results ->
  iar results.dishonest_share results.total_d results.total_h 800_000_000.0

let rsd_indicator : processed_simu_results -> float =
 fun results ->
  (results.total_d /. results.dishonest_share)
  -. (results.total_h /. results.honest_share)

let ar_vs_honest : processed_simu_results -> float =
 fun results ->
  (* This is a temporary hack relying on knowledge from the protocols. *)
  (* /!\ HACK /!\ *)
  let honest = results.length *. 80.0 *. results.dishonest_share in
  results.total_d -. honest

let compute_indicator : indicator -> processed_simu_results -> float * float =
 fun indicator results ->
  let x = results.dishonest_share in
  let y =
    match indicator with
    | AR ->
        ar_indicator results
    | IAR ->
        iar_indicator results
    | RSD ->
        rsd_indicator results
    | AR_vs_honest ->
        ar_vs_honest results
  in
  (x, y)

let () =
  Format.printf "%a" pp_mode mode ;
  match mode with
  | Generate_data
      ( { adversary;
          protocol;
          undelegated;
          dishonest_health;
          honest_health;
          seed;
          nsamples;
          debug } as parameters ) ->
      let shares =
        let upper_bound = min (1. -. undelegated) 0.7 in
        Numerics.Float64.Vec.(to_array (linspace 0.1 upper_bound 29))
        |> Array.to_list
        (* |> (fun l -> List.sort compare @@ [0.499; 0.500001; 0.501] @ l) *)
        |> List.map (fun dishonest_share ->
               let honest_share = 1. -. undelegated -. dishonest_share in
               (honest_share, dishonest_share))
      in
      let () =
        match seed with
        | None ->
            Random.self_init ()
        | Some seed ->
            Random.init seed
      in
      let results =
        Parmap.parmap
          (fun (honest, dishonest) ->
            let module S = Instantiate_all (struct
              let proto =
                match protocol with
                | Emmy_plus ->
                    ( Protocol.emmy_plus Protocol.default_emmy_B_parameters
                      :> (module Protocol.S) )
                | Emmy_sharp ->
                    ( Protocol.chambart_plus Protocol.default_emmy_C_parameters
                      :> (module Protocol.S) )

              module Proto = (val proto)

              let undelegated = undelegated

              let honest_health = honest_health

              let dishonest_health = dishonest_health

              let honest_share = honest

              let dishonest_share = dishonest

              let nsamples = nsamples (* # chains *)

              let adversary = adversary

              let debug_log = debug
            end) in
            {
              empirical = S.simulation ~time:simulation_time;
              honest_share = honest;
              dishonest_share = dishonest;
            })
          (Parmap.L shares)
      in
      let filename = filename_from_parameters parameters in
      save_simulation_results filename (parameters, results)
  | Compute_indicator {indicator; data; show} ->
      let data = List.map load_simulation_results data in
      let data = arrange_by_protocol data in
      let data =
        Protocol_map.map (Adversary_map.map process_empirical_data) data
      in
      let plottable_data =
        Protocol_map.map
          (Adversary_map.map (List.map (compute_indicator indicator)))
          data
      in
      generic_plot show indicator plottable_data
