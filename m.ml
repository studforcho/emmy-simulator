(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2019 Nomadic Labs. <nomadic@tezcore.com>                    *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

open StaTz

(* Sampling-based probability monad *)

type 'a t = 'a Stats.gen

let ( >>= ) m (f : 'a -> 'b t) =
  Stats.generative ~sampler:(fun () ->
      let m = Stats.sample_gen m in
      Stats.sample_gen (f m))

let ( >|= ) m (f : 'a -> 'b) =
  let m = Stats.sample_gen m in
  f m

let map f m = Stats.map_gen f m

let return x = Stats.generative ~sampler:(fun () -> x)

let run = Stats.sample_gen

let choice p a b =
  Stats.generative ~sampler:(fun () ->
      let x = Random.float 1.0 in
      if x < p then Stats.sample_gen a else Stats.sample_gen b)

module type Map_S = sig
  include Map.S

  val fold_m :
    (key -> 'e -> 'acc -> 'acc Stats.gen) -> 'e t -> 'acc -> 'acc Stats.gen
end

module Make_map (X : Map.OrderedType) : Map_S with type key = X.t = struct
  include Map.Make (X)

  let fold_m :
      (key -> 'e -> 'acc -> 'acc Stats.gen) -> 'e t -> 'acc -> 'acc Stats.gen =
   fun f m acc ->
    fold (fun k e acc -> acc >>= fun acc -> f k e acc) m (return acc)
end

let list_map_m : ('a -> 'b t) -> 'a list -> 'b list t =
 fun f l ->
  Stats.generative ~sampler:(fun () -> List.map (fun x -> run (f x)) l)
